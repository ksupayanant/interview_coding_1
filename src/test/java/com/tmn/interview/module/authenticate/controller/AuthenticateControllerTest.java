package com.tmn.interview.module.authenticate.controller;

import com.tmn.interview.exception.ExceptionHandling;
import com.tmn.interview.module.authenticate.service.AuthenticateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@ExtendWith({MockitoExtension.class})
class AuthenticateControllerTest {

    private MockMvc mockMvc;

    @Mock
    private AuthenticateService authenticateService;

    @BeforeEach
    void beforeEach() {
        mockMvc = standaloneSetup(new AuthenticateController(authenticateService))
                .setControllerAdvice(new ExceptionHandling())
                .build();
    }


    @Test
    void authenticate_Success() {
        //TODO <Optional> - Create unit test for authenticate success get token from api
    }


    @Test
    void authenticate_Fail_Invalid_Password() {
        //TODO <Optional> - Create unit test for authenticate fail invalid password

    }

    @Test
    void authenticate_Fail_Null_Username() {
        //TODO <Optional> - Create unit test for authenticate fail null parameter username

    }

    @Test
    void authenticate_Fail_Null_Password() {
        //TODO <Optional> - Create unit test for authenticate fail null parameter password

    }

    @Test
    void authenticate_Fail_System_Error() {
        //TODO <Optional> - Create unit test for authenticate fail system error

    }


}
