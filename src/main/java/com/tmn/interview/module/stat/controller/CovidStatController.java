package com.tmn.interview.module.stat.controller;

import com.tmn.interview.module.ApiRoot;
import com.tmn.interview.module.stat.model.CovidStatModel;
import com.tmn.interview.module.stat.service.CovidStatService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@RequiredArgsConstructor
@Log4j2
public class CovidStatController implements ApiRoot {

    private final CovidStatService covidStatService;

    @GetMapping("/covid/stat/province/maximum")
    public CovidStatModel.GetCovidStatMaximum.Response getCovidStatMaximumProvince (){
        //TODO - add validation request when null request parameter and escape special character in parameter

        return covidStatService.getCovidStatMaximumProvince();
    }

}
