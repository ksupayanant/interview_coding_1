package com.tmn.interview.module.stat.service;

import com.tmn.interview.client.ddc.Covid19DdcClient;
import com.tmn.interview.module.stat.model.CovidStatModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

@Log4j2
@Service
@RequiredArgsConstructor
public class CovidStatService {

    private final Covid19DdcClient covid19DdcClient;

    public CovidStatModel.GetCovidStatMaximum.Response getCovidStatMaximumProvince() {
        //TODO - add logic for get data from API guildline in README.md to calculate response and Handle exception follow unit test case

        return CovidStatModel.GetCovidStatMaximum.Response.builder().build();
    }
}
